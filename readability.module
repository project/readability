<?php

/**
 * @file
 * Analyzes node content for search engine optimization recommendations
 */

define( 'READABILITY_DEFAULT_TARGETGRADE_MIN', 6 );
define( 'READABILITY_DEFAULT_TARGETGRADE_MAX', 12 );
 
/**
 * Implementation of hook_menu().
 */
function readability_menu() {
  $items = array();
  $items['admin/settings/readability'] = array(
    'title' => t('Readability'),
    'description' => 'Grades readability of content.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('readability_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('admin content analysis'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'readability.admin.inc',
  );
  
  return $items;
}

/**
 *  Implementation of hook_contentalalysis_analyzers()
 *  
 */
function readability_contentanalysis_analyzers() {
  $analyzers['readability'] = array(
    'title' => t('Readability'),
    'module' => 'readability',
    'callback' => 'readability_analyzer',
    'weight' => -5,
  ); 
  return $analyzers;  
}

/**
 * Implementation of hook_analyzer() via custom define callback
 * 
 * Analyzes content based on readability algorithms from php-text-statistics
 * 
 * @rerturn array
 *   readability analysis
 */
function readability_analyzer($context, $analysis, $params) {

  $file = './' . drupal_get_path('module', 'readability') . '/php-text-statistics/TextStatistics.php';
  if (!file_exists($file)) {
    $msg = t('The readability module requires the open source php-text-statistics class. ');
    $msg .= l(t('Download the class here.'), 'http://code.google.com/p/php-text-statistics/', array('attributes' => array('target' => '_phptextstats')));
    $msg .= "<br><br>";
    $msg .= t(' Download the files and place them in a folder named "php-text-statistics" under the readability module directory.');
    $analysis['messages'][] = contentanalysis_format_message($msg, 'error');
    return $analysis;
  }
  include($file);
  
  // analyze body
  $body = strtolower($context['body']);
  $body_notags = strip_tags($body);
  
  $textStatistics = new TextStatistics();
  $stats = array();
  $analysis['body']['stats'] = array();
  
  $gmin = variable_get('readability_targetgrade_min', READABILITY_DEFAULT_TARGETGRADE_MIN);
  $gmax = variable_get('readability_targetgrade_min', READABILITY_DEFAULT_TARGETGRADE_MAX);
  
  //$stats['flesch_kincaid_reading_ease'] = $textStatistics->flesch_kincaid_reading_ease($body_notags); 
  //$analysis['body']['stats']['flesch_kincaid_reading_ease'] = contentanalysis_format_stat(t('Flesch Kincaid reading ease'),$stats['flesch_kincaid_reading_ease'],1);
  
  $stats['flesch_kincaid_grade_level'] = $textStatistics->flesch_kincaid_grade_level($body_notags);
  $stats['gunning_fog_score'] = $textStatistics->gunning_fog_score($body_notags); 
  $stats['coleman_liau_index'] = $textStatistics->coleman_liau_index($body_notags); 
  $stats['smog_index'] = $textStatistics->smog_index($body_notags);
  $stats['automated_readability_index'] = $textStatistics->automated_readability_index($body_notags); 

  $statuses = array();
  $total = 0;
  foreach ($stats AS $k => $v) {
    $total += $v;
    $statuses[$k] = 'status';
    if (($v < $gmin) || ($v > $gmax)) {
      $statuses[$k] = 'warning';
    }
  }

  $names = array(
    'flesch_kincaid_grade_level' => t('Flesch Kincaid'),
    'gunning_fog_score' => t('Gunning Fog Score'),
    'coleman_liau_index' => t('Coleman Liau Index'),
    'smog_index' => t('SMOG Index'),
    'automated_readability_index' => t('Automated Readability Index'),  
  );  
  
  foreach ($stats AS $k => $v) {
    $analysis['body']['messages'][] = contentanalysis_format_message($names[$k] . ": " . number_format($v, 1), $statuses[$k]);
  }
  
  $stats['average'] = $total/5; 
  $statuses['average'] = 'complete';
  $analysis['#status'] = 'complete';
  $analysis['body']['#status'] = 'complete';
  if (($stats['average'] < $gmin) || ($stats['average'] > $gmax)) {
    $statuses['average'] = 'warning';
    $analysis['#status'] = 'warning';
    $analysis['body']['#status'] = 'warning';
  } 
  $analysis['#score'] = $stats['average'];
  
  $analysis['body']['messages'][] = contentanalysis_format_message(t('<strong>Average: %average</strong>', array('%average' => number_format($stats['average'], 1))), $statuses['average']);
  
  
  $analysis['#context']['analyzer_data']['readability'] = $stats;
  
  return $analysis;
}